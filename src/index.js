const express = require("express");
const connection = require("./db/mongoose");
require('dotenv').config()
const port = process.env.PORT;
const cors = require("cors");
const userRouter = require("./routers/user");
const tweetRouter = require('./routers/tweet')

connection();

const app = express();
app.use(cors());


app.use(express.json());
app.use(userRouter);
app.use(tweetRouter);

app.listen(port, () => {
  console.log(`Server is up on ${port}`);
});