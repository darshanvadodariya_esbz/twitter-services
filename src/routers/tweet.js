const express = require('express')
const Tweet = require('../models/tweet')
const auth = require('../middleware/auth')
const multer = require('multer')
const router = new express.Router()

const upload = multer({
    limits: {
        fileSize: 1000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
            return cb(new Error('Please upload an image'))
        }

        cb(undefined, true)
    }
})

router.post('/tweets', auth, upload.single('tweetMedia'), async (req, res) => {
    const tweet = new Tweet({
        owner: req.user._id,
        description: req.body.description,
        tweetMedia: (req.file && req.file.buffer),
    })
    try {
        await tweet.save()
        res.status(201).send(tweet)
    } catch (e) {
        res.status(400).send(e)
    }
})





module.exports = router