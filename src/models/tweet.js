const mongoose = require('mongoose')
const tweetSchema = new mongoose.Schema({
  description: {
    type: String,
    trim: true
  },
  tweetMedia: {
    type: Buffer
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    require: true,
    ref: 'User'
  },
}, {
  timestamps: true
})


const Tweet = mongoose.model('Tweet', tweetSchema)

module.exports = Tweet